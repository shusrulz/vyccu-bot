##happy_path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
    - slot{"requested_slot": "name"}
    - slot{"requested_slot": "account_number"}
    - form{"name": null}
    - utter_slots_values
* goodbye
    - utter_goodbye
## unhappy path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
* chitchat
    - utter_chitchat
    - bank_form
    - form{"name": null}
    - utter_slots_values
* goodbye
    - utter_goodbye

## very unhappy path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
* chitchat
    - utter_chitchat
    - bank_form
* chitchat
    - utter_chitchat
    - bank_form
* chitchat
    - utter_chitchat
    - bank_form
    - form{"name": null}
    - utter_slots_values
* goodbye
    - utter_goodbye

## stop but continue path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
* stop
    - utter_ask_continue
* affirm
    - bank_form
    - form{"name": null}
    - utter_slots_values
* goodbye
    - utter_goodbye

## stop and really stop path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
* stop
    - utter_ask_continue


## chitchat stop but continue path
* register_account
    - bank_form
    - form{"name": "bank_form"}
* chitchat
    - utter_chitchat
    - bank_form
* stop
    - utter_ask_continue
* affirm
    - bank_form
    - form{"name": null}
    - utter_slots_values
* goodbye
    - utter_goodbye

## stop but continue and chitchat path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
* stop
    - utter_ask_continue
* affirm
    - bank_form
* chitchat
    - utter_chitchat
    - bank_form
    - form{"name": null}
    - utter_slots_values
* goodbye
    - utter_goodbye

## chitchat stop but continue and chitchat path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
* chitchat
    - utter_chitchat
    - bank_form
* stop
    - utter_ask_continue
* affirm
    - bank_form
* chitchat
    - utter_chitchat
    - bank_form
    - form{"name": null}
    - utter_slots_values
* goodbye
    - utter_goodbye

## chitchat, stop and really stop path
* greet
    - utter_greet
* register_account
    - bank_form
    - form{"name": "bank_form"}
* chitchat
    - utter_chitchat
    - bank_form
* stop
    - utter_ask_continue


## story1
* greet
  - utter_greet

##story2
* out_of_scope
- utter_out_of_scope

##story3
* goodbye
- utter_goodbye

##story4
* about_loan
- utter_about_loan

## story_01
* about_loan{"type_of_loan":"business loan"}
- slot{"type_of_loan":"business loan"}
- fetchtypeofloan
- slot{"type_of_loan":"business loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"business loan"}
    - slot{"type_of_loan":"business loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"business loan"}

## New Story

* about_loan{"type_of_loan":"business loan"}
    - slot{"type_of_loan":"business loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"business loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}

## New Story

* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}

## New Story

* about_loan
    - utter_about_loan
     - actionshowtypeofloan
* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}

## New Story

* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"education loan"}
    - slot{"type_of_loan":"education loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"education loan"}

## New Story

* about_loan{"type_of_loan":"education loan"}
    - slot{"type_of_loan":"education loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"education loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"hire purchase loan"}
    - slot{"type_of_loan":"hire purchase loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"hire purchase loan"}

## New Story

* about_loan{"type_of_loan":"hire purchase loan"}
    - slot{"type_of_loan":"hire purchase loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"hire purchase loan"}

## New Story

* about_loan
    - utter_about_loan
     - actionshowtypeofloan
* about_loan{"type_of_loan":"home loan"}
    - slot{"type_of_loan":"home loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"home loan"}

## New Story

* about_loan{"type_of_loan":"home loan"}
    - slot{"type_of_loan":"home loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"home loan"}

## New Story

* about_loan
    - utter_about_loan
    - utter_about_loan
* about_loan{"type_of_loan":"industry loan"}
    - slot{"type_of_loan":"industry loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"industry loan"}

## New Story

* about_loan{"type_of_loan":"industry loan"}
    - slot{"type_of_loan":"industry loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"industry loan"}

## New Story
* about
- utter_about

##New Story
*location
- utter_location

##New Story
*contact
- utter_contact

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"business loan"}
    - slot{"type_of_loan":"business loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"business loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"education loan"}
    - slot{"type_of_loan":"education loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"education loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"hire purchase loan"}
    - slot{"type_of_loan":"hire purchase loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"hire purchase loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"home loan"}
    - slot{"type_of_loan":"home loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"home loan"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"industry loan"}
    - slot{"type_of_loan":"industry loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"industry loan"}

## New Story

* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}

## New Story

* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}

## New Story

* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}

## New Story

* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}

## New Story

* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"remittance"}
    - slot{"type_of_services":"remittance"}
    - fetchtypeofservices
    - slot{"type_of_services":"remittance"}

## New Story

* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"deposit"}
    - slot{"type_of_services":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":"deposit"}

## New Story

    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}

## New Story

    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}

## New Story

    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}

## New Story

    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}

## New Story

    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}

## New Story

    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}

## New Story

* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}

## New Story

* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}

## New Story

    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}

## New Story

    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}

## New Story

* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}

## New Story

* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}

## New Story

    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}

## New Story

    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}

## New Story

* contact
    - utter_contact

## New Story

* greet
    - utter_greet
* contact
    - utter_contact

## New Story

* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}

## New Story

* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}

## New Story

    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}

## New Story

    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}

##New Story
* about_service_center
 - utter_about_service_center

## New Story

* about_service_center
    - utter_about_service_center

## New Story

* about_service_center
    - utter_about_service_center
* greet
    - utter_greet

## New Story

* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}

## New Story

    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}

## New Story

    - utter_greet
* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_greet
* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"deposit"}
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
* goodbye
	- utter_goodbye

## New Story
* greet
    - utter_greet
* interest_rate
    - utter_about_interest_rate
    - actionshowtypeofinterest
* interest_rate{"type_of_interest":"loan"}
    - slot{"type_of_interest":"loan"}
    - fetchtypeofinterest
    - slot{"type_of_interest":"loan"}
* goodbye
	- utter_goodbye

## New Story

* greet
    - utter_greet
* location
    - utter_location
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_greet
* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"career loan"}
    - slot{"type_of_loan":"career loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"career loan"}
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"remittance"}
    - slot{"type_of_services":"remittance"}
    - fetchtypeofservices
    - slot{"type_of_services":"remittance"}
* goodbye
    - utter_goodbye

## New Story

    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"deposit"}
    - slot{"type_of_services":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":"deposit"}
* goodbye
    - utter_goodbye

## New Story
* greet
    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}
* goodbye
    - utter_goodbye

## New Story
* greet
    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"ATM"}
    - slot{"type_of_services":"ATM"}
    - fetchtypeofservices
    - slot{"type_of_services":"ATM"}
* goodbye
    - utter_goodbye

## New Story
* greet
    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"SMS banking"}
    - slot{"type_of_services":"SMS banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"SMS banking"}
* goodbye
    - utter_goodbye

## New Story
* greet
    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}
* goodbye
    - utter_goodbye

## New Story
* greet
    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"deposit"}
    - slot{"type_of_services":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":"deposit"}
* goodbye
    - utter_goodbye

## New Story
* greet
    - utter_greet
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"remittance"}
    - slot{"type_of_services":"remittance"}
    - fetchtypeofservices
    - slot{"type_of_services":"remittance"}
* goodbye
    - utter_goodbye

## New Story

    - utter_location
* services
    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_services":"sms banking"}
    - slot{"type_of_services":"sms banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"sms banking"}

## New Story

* out_of_scope
    - utter_out_of_scope

## New Story

* greet
    - utter_greet
* contact
    - utter_contact

## New Story

    - utter_about_services
    - actionshowtypeofservices
* services{"type_of_interest":"deposit"}
    - slot{"type_of_interest":"deposit"}
    - fetchtypeofservices
    - slot{"type_of_services":null}

## New Story

* services{"type_of_services":"sms banking"}
    - slot{"type_of_services":"sms banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"sms banking"}

## New Story

* services{"type_of_services":"mobile banking"}
    - slot{"type_of_services":"mobile banking"}
    - fetchtypeofservices
    - slot{"type_of_services":"mobile banking"}

## New Story

* services{"type_of_services":"atm"}
    - slot{"type_of_services":"atm"}
    - fetchtypeofservices
    - slot{"type_of_services":"atm"}

## New Story

* services{"type_of_services":"health insurance"}
    - slot{"type_of_services":"health insurance"}
    - fetchtypeofservices
    - slot{"type_of_services":"health insurance"}

## New Story

* about_loan
    - utter_about_loan
    - actionshowtypeofloan
* about_loan{"type_of_loan":"agriculture loan"}
    - slot{"type_of_loan":"agriculture loan"}
    - fetchtypeofloan
    - slot{"type_of_loan":"agriculture loan"}
