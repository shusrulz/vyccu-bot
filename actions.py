# This files contains your custom actions which can be used to run
# custom Python code.
from typing import Dict, Text, Any, List, Union, Optional
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from rasa_sdk.forms import FormAction


class ActionShowTypeOfLoan(Action):
    def name(self):
        return 'actionshowtypeofloan'
    def run(self, dispatcher, tracker,domain):
        # loan_type = tracker.get_slot('agriculture loan')
        message = "we provide following type of loans"
        buttons = [{'title': 'agriculture loan',
                   'payload': '{}'.format('agriculture loan')},
                  {'title': 'career loan',
                   'payload': '{}'.format('career loan')},
                  {'title': 'education loan',
                   'payload': '{}'.format('education loan')},
                  {'title': 'hire purchase loan',
                   'payload': '{}'.format('hire purchase loan')},
                  {'title': 'home loan',
                   'payload': '{}'.format('home loan')},
                  {'title': 'business loan',
                   'payload': '{}'.format('business loan')},
                  {'title': 'industry loan',
                   'payload': '{}'.format('industry loan')}   
                   ]
        dispatcher.utter_button_message(message, buttons=buttons)


class ActionShowTypeOfServices(Action):
    def name(self):
        return 'actionshowtypeofservices'
    def run(self, dispatcher, tracker,domain):
        message = "we provide following type of services"
        buttons = [{'title': 'ATM',
                   'payload': '{}'.format('ATM')},
                  {'title': 'SMS banking',
                   'payload': '{}'.format('SMS banking')},
                  {'title': 'mobile banking',
                   'payload': '{}'.format('mobile banking')},
                  {'title': 'hire purchase loan',
                   'payload': '{}'.format('hire purchase loan')},
                  {'title': 'health insurance',
                   'payload': '{}'.format('health_insurance')},
                  {'title': 'deposit',
                   'payload': '{}'.format('deposit')}   
                   ]
        dispatcher.utter_button_message(message, buttons=buttons)

class FetchTypeOfServices(Action):
	def name(self):
		return 'fetchtypeofservices'

	def run(self,dispatcher,tracker,domain):
		service_type = tracker.get_slot('type_of_services')
		service_link = "http://vyccu.org.np/services/"
		response = "We provide attractive {} services. You can get more info on {} from {}".format(service_type,service_type,service_link)
		dispatcher.utter_message(response)
		return [SlotSet('type_of_services', service_type)]

class FetchTypeOfLoan(Action):
	def name(self):
		return 'fetchtypeofloan'

	def run(self,dispatcher,tracker,domain):
		loan_type = tracker.get_slot('type_of_loan')
		loan_link = "http://vyccu.org.np/service/loans/"
		response = "We provide attractive {} loans. You can get more info on {} from {}".format(loan_type,loan_type,loan_link)
		dispatcher.utter_message(response)
		return [SlotSet('type_of_loan', loan_type)]

class ActionShowTypeOfInterest(Action):
    def name(self):
        return 'actionshowtypeofinterest'
    def run(self, dispatcher, tracker,domain):
        message = "Interest rate varies for following services"
        buttons = [{'title': 'loan',
                   'payload': '{}'.format('loan')},
                  {'title': 'deposit',
                   'payload': '{}'.format('deposit')}   
                   ]
        dispatcher.utter_button_message(message, buttons=buttons)

class FetchTypeOfInterest(Action):
	def name(self):
		return 'fetchtypeofinterest'

	def run(self,dispatcher,tracker,domain):
		interest_type = tracker.get_slot('type_of_interest')
		interest_link = "http://vyccu.org.np/"
		response = "You can check varying interest rate for {} on  {}".format(interest_type,interest_link)
		dispatcher.utter_message(response)
		return [SlotSet('type_of_interest', interest_type)]

class BankForm(FormAction):
    """Example of a custom form action"""

    def name(self) -> Text:
        """Unique identifier of the form"""

        return "bank_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["name",  "account_number"]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "name": [self.from_entity(entity="name"), self.from_text()],
            "account_number": [self.from_entity(entity="account_number"), self.from_text()],
        }
    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        # utter submit template
        dispatcher.utter_template("utter_submit", tracker)
        return []
